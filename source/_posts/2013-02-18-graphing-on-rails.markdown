---
layout: post
title: "Graphing on Rails"
date: 2013-02-18 14:51
comments: true
categories: [Rails, Charts, Graphs]
---
Took some time to research the best way to graph with Rails.

<!-- more -->

## Principles
* We aren't plotting a dozen huge time series here- it's just a couple of bar graphs. Let's **take it easy.**
* We are amateurs; **easy is better.** Must have good online examples in Rails.
* Whatever we pick should be **popular and have a big help community.**

## Requirements
* It has to be **free** for commercial use
* We have to be able to let users **easily print pretty graphs**
* Users don't need to be able to save graphs; they can be dynamically built

## Research
There are plenty of client-side graphic libraries built entirely in javascript. The most popular, by all accounts, is [HighCharts](http://www.highcharts.com/), which is unfortunately not free for commercial use. This is sad because there's a great [RailsCast Episode](http://railscasts.com/episodes/223-charts) showing how to use it.

For a list of way too many graphing libraries, check out [this matrix.](http://socialcompare.com/en/comparison/javascript-graphs-and-charts-libraries) 

For a list of Ruby gems and their relative popularity, see [here.](https://www.ruby-toolbox.com/projects/google_visualr).

From the above, some quora posts and stackoverflow, here are the finalists:

* [Google Charts](https://developers.google.com/chart/) (for which there are multiple gems)
* [Flot](http://www.flotcharts.org/)
* [Raphael](http://g.raphaeljs.com/)
* [D3.js](http://d3js.org/)
