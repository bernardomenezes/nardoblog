---
layout: post
title: "Adding logging to Heroku"
date: 2013-02-18 17:59
comments: true
categories: Heroku
---

How to add a logging tool to Heroku and set up simple alerts

<!-- more -->

##Get your free logging

One of the obvious things to install once we "go live" in Heroku is a logging tool. We'll need it to examine logs, analyze traffic, and provide alerts should something go wrong. Heroku has 4 options available: Loggly, Papertrail, LogEntries, and FlyData Beta.

I initially went with Loggly for a couple of reasons: 

* They seem to be the most widely used cloud logging tool (or so they claim) 
* They have Amazon S3 integration (which at least Papertrail also has, if not others)
* We're not looking for that much functionality, so I didn't look too much harder than that!

Installing Loggly was a breeze- I did it through Heroku's graphic interface and was immediately up and running. The free version only stores logs for a day, so I made a quick visit to their documentation and [connected Loggly to Amazon S3.](http://loggly.com/support/advanced/s3-bucket-archives/)

This satisfied almost all of my needs. Almost. 

Unfortunately, Loggly doesn't do alerts in Heroku. (See bottom of [this page](http://loggly.com/support/using-data/alerting-with-alert-birds/))

LogEntries doesn't seem to have S3 integration, FlyData is in beta, so Papertrail it is. 

## Papertrail

Uninstalling Loggly and installing Papertrail was as easy as a few clicks on the Heroku graphical dashboard. Once again, I followed the developer's [directions](http://help.papertrailapp.com/kb/how-it-works/permanent-log-archives#bucket) for permanent archive logging at Amazon S3. 

Unlike Loggly, Papertrail positively confirmed that it had connected appropriately to my S3 bucket. Unlike Loggly, I could not choose the log format. It automatically dumps daily logs in tab-separated value format, gzip compressed. 

Papertrail's dashboard interface is quite beautiful. (screenshots below)

## Setting up alerts

I'm not sure where to find a comprehensive list of alerts that I should be looking for. An easy one is 404 errors. A high incidence would tell me that perhaps I have mislinked something on the page. As an exercise, I set up a daily email alert with number of 404 errors found.

### Create the query

To do that, go to the "events" tab of the Papertrail dashboard and enter in the query:

```
status=404
```

This will list all of the requests that hit error 404:
{% img images/papertrail_query.jpeg %}


From there, it's just a matter of selecting the "save search" button and configuring the alert settings:

{% img images/papertrail_alert.jpeg %}




